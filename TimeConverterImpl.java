package com.paic.arch.interviews;

import com.sun.javafx.image.BytePixelSetter;

public class TimeConverterImpl implements  TimeConverter {

    private int light1S;
    private int light5H;
    private int light1H;
    private int light5Min;
    private int light1Min;


    @Override
    public String convertTime(String aTime) {
        String[] time = aTime.split(":");
        StringBuilder out = new StringBuilder();
        if(time.length != 3){
            return null;
        }
        int hour = Integer.parseInt(time[0]);
        int minute = Integer.parseInt(time[1]);
        int second = Integer.parseInt(time[2]);

        this.light1S = second%2;
        this.light5H = hour/5;
        this.light1H = hour%5;
        this.light5Min = minute/5;
        this.light1Min = minute%5;

        //2秒闪烁一次的灯
        out.append(this.light1S==0?"Y":"O" );
        out.append("\r\n");

        //每一盏5小时的灯
        for (int i=0; i<4; i++){
            if(i<light5H){
                out.append("R");
            }else{
                out.append("O");
            }
        }
        out.append("\r\n");

        //每一盏1小时的灯
        for (int i=0; i<4; i++){
            if(i<light1H){
                out.append("R");
            }else{
                out.append("O");
            }
        }
        out.append("\r\n");

        //每一盏5分钟的灯
        for (int i=0; i<11; i++){
            if(i<light5Min){
                if((i+1)%3==0){
                    out.append("R");
                }else {
                    out.append("Y");
                }
            }else {
                out.append("O");
            }
        }
        out.append("\r\n");

        //每一盏1分钟的灯
        for(int i=0; i<4; i++){
            if (i<light1Min){
                out.append("Y");
            }else {
                out.append("O");
            }
        }
        out.append("\r\n");

        return out.toString();
    }

    public static void main(String[] args){
        System.out.print(new TimeConverterImpl().convertTime("24:00:00"));
    }
}
